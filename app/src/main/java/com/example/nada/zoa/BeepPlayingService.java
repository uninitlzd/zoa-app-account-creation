package com.example.nada.zoa;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by me on 23/11/17.
 */

public class BeepPlayingService extends Service{
    int startId;
    boolean isRunning;
    MediaPlayer media_song;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        String state = intent.getExtras().getString("extra");

        Log.e("Ringtone state:extra is", state);

        switch (state) {
            case "remind on":
                startId = 1;
                break;
            case "remind off":
                startId = 0;
                break;
            default:
                startId = 0;
                break;
        }

        if (!this.isRunning && startId == 1){
            Log.e("there is no music", "and you want start");
            media_song = MediaPlayer.create(this, R.raw.beep);
            media_song.start();

            this.isRunning = true;
            this.startId = 0;

            NotificationManager notif_man = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            Intent intent_routine_activity = new Intent(this.getApplicationContext(), routine_Activity.class);

            PendingIntent pendingIntent_routineActivity = PendingIntent.getActivity(this, 0, intent_routine_activity,0);

            Notification notification_remind = new Notification.Builder(this)
                    .setContentTitle("Alarm is going off")
                    .setContentText("Click me")
                    .setContentIntent(pendingIntent_routineActivity)
                    .setSmallIcon(R.drawable.notif)
                    .setAutoCancel(true)
                    .build();

            notif_man.notify(0, notification_remind);


        } else if (this.isRunning && startId == 0){
            Log.e("there is music", "and you want end");
            media_song.stop();
            media_song.reset();

            this.isRunning = false;
            this.startId = 0;
        } else if (!this.isRunning && startId == 0){
            Log.e("there is music", "and you want start");

            this.isRunning = true;
            this.startId = 0;
        } else if (this.isRunning && startId == 1){
            Log.e("there is music", "and you want start");

            this.isRunning = true;
            this.startId = 1;
        } else {
            Log.e("else", "somehow you reached this");
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.e("On Destroy called", "ta da");

        super.onDestroy();
        this.isRunning=false;
    }

}
